from Models.Services import *
from Providers import *

# import sys
# from env import *
# sys.path.append( PROJECT_PATH+'/Providers' )
# import Providers.ViewProvider as ViewProvider

def verify_item(item):
	if ( item.lower()!='n' ):
		return True
	else:
		return False


# MainController
class MainController:

	# 取得Service Instance
	atmAccountService = AtmAccountService()
	# 取得ViewProvider Instance
	ViewProvider = ViewProvider()
	# 定義渲染函式
	view = ViewProvider.view

	def login( self,obj ):
		
		data = self.atmAccountService.getValidAccount()
		compact_data = { 'account_number':data.account_number }
		# compact_data = { 'account_number':'001051111222' }
		self.view( 'layouts.login_page',compact_data )

		# 輸入
		input_value = input('')

		check = True

		if check:
			obj.user_data = data
			return 'menu'
		else:
			return False


	def menu( self,obj ):
		self.view( 'layouts.main_page' )
		# 輸入
		input_value = input('')

		menu_route = {
			'1':'balance_inquiries',
			'2':'withdrawals',
			'3':'transfer',
			'4':'deposit',
			'5':False
		}

		return menu_route.get( input_value,'menu' )


	def show_account_balance( self,obj ):

		# data = self.atmAccountService.getAccountData(1) #'001051111222','balance'
		# 取得帳戶資訊
		# data = obj.user_data
		data = self.atmAccountService.getValidAccount()
		chnum = Number2String(data.balance)
		compact_data = { 'balance':data.balance,'balance_hz':chnum }

		self.view( 'layouts.account_balance',compact_data )
		input_value = input('')

		return 'menu'


	#item1
	def balance_inquiries( self,obj ):

		# data = self.atmAccountService.getAccountData('001051111222') #'001051111222'
		# 取得帳戶資訊
		data = obj.user_data
		# data = self.atmAccountService.getValidAccount()

		compact_data = { 'account': data.account_number }
		# compact_data = { 'account':user1[0]['aid'] }

		# 畫面輸出
		self.view( 'layouts.balance_inquiries',compact_data )

		input_item = input('')
		if (verify_item(input_item)):
			return 'show_account_balance'
		else:
			return False


	# item2
	def withdrawals( self,obj ):

		# 取得帳戶資訊
		data = obj.user_data

		# withdrawals_money_type = {1: 1000, 2: 2000, 3: 5000, 4: 10000, 5: 50000}
		withdrawals_money_type = { 1:1000, 2:2000, 3:5000, 4:10000, 5:50000 }
		compact_data = withdrawals_money_type

		# 畫面輸出
		self.view( 'layouts.withdrawals',compact_data )

		menu_item = eval(input(''))
		if(menu_item==9):
			return False
		elif(menu_item==6):
			# 畫面輸出
			self.view( 'layouts.withdrawals_amount' )
			withdrawal_money = eval(input(''))

		else:
			if (menu_item==1):
				# 畫面輸出
				self.view( 'layouts.withdrawals_change_bills' )
				input_item = input('')
				# handle function for 10 百元鈔 exchange


			withdrawal_money = withdrawals_money_type[menu_item]
			compact_data = { 'withdrawal_money':withdrawal_money }

		# 畫面輸出
		self.view( 'layouts.withdrawals_check',compact_data )


		input_item = input('')

		if (verify_item(input_item)):
			temp_balance = data.balance - withdrawal_money
			if ( withdrawal_money<100 ):
				# 畫面輸出
				self.view( 'layouts.withdrawals_lessThanHundred' )
				return False

			elif ( withdrawal_money>data.balance ):
				# 畫面輸出
				self.view( 'layouts.withdrawals_moreThanRemains' )
				return False

			else:
				# data.balance = data.balance-withdrawal_money
				obj.user_data = self.atmAccountService.withdrawMoney( data,withdrawal_money )
				# 更新帳戶變數
				 # = self.atmAccountService.getValidAccount()
				return 'show_account_balance'
		else:
			return False

	#item3
	def transfer( self,obj ):

		# 取得帳戶資訊
		data = obj.user_data

		# 畫面輸出
		self.view( 'layouts.transfer_account' )
		t_account = input('')

		# 畫面輸出
		self.view( 'layouts.transfer_amount' )
		t_price = eval(input(''))

		# t_account = input(u"請輸入要轉入的帳號:")
		# t_price = eval(input(u"轉入的金額:"))
		# new_user = [{'aid':t_account,'pws':'1234','balance':t_price}]
		# print (u"\n您要轉入的帳號為  ",new_user[0]['aid'])
		# print (u"轉入的金額是  ",new_user[0]['balance'],"\n")

		# 畫面輸出
		# self.view("middle",['','您要轉入的帳號為:'+str(new_user[0]['aid']),'轉入的金額是'+str(new_user[0]['balance']),'請問是否進行轉帳 Y/N?',''])
		compact_data = { 'new_user':t_account,'balance':t_price }
		self.view( 'layouts.transfer_check',compact_data )

		input_item = input("")
		# input_item = input(u"請問是否進行轉帳,Y/N?")
		if (verify_item(input_item)):
			obj.user_data = self.atmAccountService.transferMoney( data,t_price )
			return 'show_account_balance'
		else:
			return False

	# item4
	def deposit( self,obj ):

		# 取得帳戶資訊
		data = obj.user_data

		# 畫面輸出
		compact_data = { 'account':data.account_number }
		self.view( 'layouts.deposit_amount',compact_data )
		t_price = eval(input(''))

		# 畫面輸出
		compact_data = { 'amount':t_price }
		self.view( 'layouts.deposit_check',compact_data )


		input_item = input('')
		if (verify_item(input_item)):
			if (t_price < 1000):
				# 畫面輸出
				self.view( 'layouts.deposit_error' )
				input('')
				return False
			obj.user_data = self.atmAccountService.depositMoney( data,t_price )
			return 'show_account_balance'
		else:
			return False




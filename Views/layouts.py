
view_blade = {
	'main_page' : {
					'alignment':'middle',
					'data':[
						'邦邦銀行您好',
						'1.查詢餘額',
						'2.提款',
						'3.轉帳',
						'4.存款',
						'5.離開服務',
					]
				 },


	'login_page' : {
					'alignment':'left',
					'data':[
						'邦邦銀行您好',
						'您的帳號 {data[account_number]}',
						'請輸入您的6位數金融卡密碼：',
						'',
						'',
						'',
					]
				 },

	'account_balance' : {
						'alignment':'middle',
						'data':[
							'',
							'帳戶餘額為',
							'NT$ {data[balance]}',
							'',
							'{data[balance_hz]}',
							'',
						]
					 },

	'balance_inquiries' : {
					'alignment':'middle',
					'data':[
						'',
						'您要查詢的帳號',
						'{data[account]}',
						'Y/N?(Y)',
						'',
					]
				},

	# item2
	'withdrawals' : {
					'alignment':'left',
					'data':[
						'',
                        ' 1.{data[1]}' +
						'(可換10張百元鈔)                        6.其他金額',
						' 2.{data[2]}',
						' 3.{data[3]}',
						' 4.{data[4]}',
						' 5.{data[5]}' +
						'                                       9.離開',
						'']
				},
	'withdrawals_amount' : {
					'alignment':'middle',
					'data':[
						'',
                        '請輸入提領金額?',
                        ''
                    ]
				},
	'withdrawals_change_bills' : {
					'alignment':'middle',
					'data':[
						'',
                        '是否換成十張百元鈔票,Y/N?(Y)',
                        ''
                    ]
				},
	'withdrawals_check' : {
					'alignment':'middle',
					'data':[
						'',
                        '提領的金額是:{data[withdrawal_money]}',
                        '請問是否進行提領,Y/N?(Y)',
                        ''
                    ]
				},
	'withdrawals_lessThanHundred' : {
					'alignment':'middle',
					'data':[
						'',
                        '提領的金額不可小於100元',
                        ''
                    ]
				},
	'withdrawals_moreThanRemains' : {
					'alignment':'middle',
					'data':[
						'',
                        '提領的金額超過餘額',
                        ''
                    ]
				},

	#item3
	'transfer_account' : {
					'alignment':'middle',
					'data':[
						'',
                        '請輸入要轉入的帳號:',
                        ''
                    ]
				},
	'transfer_amount' : {
					'alignment':'middle',
					'data':[
						'',
                        '轉入的金額:',
                        ''
                    ]
				},
	'transfer_check' : {
					'alignment':'middle',
					'data':['',
							'您要轉入的帳號為:{data[new_user]}',
							'轉入的金額是{data[balance]}',
							'請問是否進行轉帳 Y/N?(Y)',
							''
					]
				},

	# item4
	'deposit_amount' : {
					'alignment':'middle',
					'data':['',
							'存款帳號為:  {data[account]}',
							'請輸入存款金額: ',
							''
					]
				},
	'deposit_check' : {
					'alignment':'middle',
					'data':['',
							'存款的金額是:  {data[amount]}',
							'請問是否進行存款,Y/N?(Y)',
							''
					]
				},
	'deposit_error' : {
					'alignment':'middle',
					'data':['',
							'臨櫃存款最低金額需1000以上!  ',
							''
					]
				},




}
# -*- coding: utf-8 -*-
# @Author: Pro.ChihCheng
# @Date:   2017-09-21 17:58:01
# @Last Modified by:   Pro.ChihCheng
# @Last Modified time: 2017-10-05 17:09:10

# Helper Functions

# Convert number to string
def Number2String(_num_):
	num_ch = {'0':'零','1': '壹', '2': '貳','3':'參','4':'肆','5':'伍','6':'陸','7':'柒','8':'捌','9':'玖'}
	num_unit = {'1': '圓整','2': '拾','3': '佰','4': '仟','5': '萬','6': '拾','7': '百','8': '千','9': '億'}
	numlist = list(str(_num_))
	strNum = ""

	count = len(str(_num_))
	for n in numlist:
		if (n!='0') :
			strNum += num_ch[n]+num_unit[str(count)] #num_ch[i]+
		elif (count==5):
			strNum += num_unit[str(count)] #num_ch[i]+
		elif (count==1):
			strNum += num_unit['1']
		count-=1

	return strNum

def compact(_list_,_dic_parameters):

	# 因為Python list的賦值PassbyReference，須使用下面方式進行複製
	compacted_list = _list_[:]

	for key,string in enumerate(compacted_list):
		compacted_list[key] = string.format( data=_dic_parameters )

	return compacted_list

import unicodedata
import subprocess
import os

from Helpers.string import *


def width_normalizer( width,msg ):
	for c in msg:
		if unicodedata.east_asian_width(c) in ('F', 'W', 'A'):
			width -= 1
	return width

def window_cleaner():
	# os.system('cls' if os.name == 'nt' else 'clear')
	subprocess.call('cls' if os.name == 'nt' else 'clear', shell=True)


# ViewProvider
class ViewProvider:

	width = 60

	row = '*{content:{align}{wide}}*\n'

	view_template = (
						"{header}\n"
						"{body}"
						"{footer}\n"
					)

	def __init__( self ):
		# 定義中間body的寬度
		self.content_width = self.width-2


	def view( self,name,compact_data=None ):
		# 清除畫面
		window_cleaner()
		# 解析名稱字串（Laravel Blade-Like）
		parse_name_array = name.split('.')
		# 新增View Name Space
		view_file = 'Views'
		# 組織Module字串
		for index in range(0,len(parse_name_array)-1):
			view_file = view_file+'.'+parse_name_array[index]
		# 動態載入：import Module字串
		view_module =__import__(view_file,fromlist='.')
		# 取得真正View Blade
		view_blade = view_module.view_blade[parse_name_array[-1]]

		# 內文套版
		if compact_data!=None:
			content = self.buildBody(view_blade['alignment'],compact(view_blade['data'],compact_data))
		else:
			content = self.buildBody(view_blade['alignment'],view_blade['data'])

		# 輸出內文
		screen_output = self.view_template.format( header='*'*self.width,footer='*'*self.width,body=content )
		print( screen_output )


	def buildBody( self,type,page_array ):

		if(type=='left'):
			align = '<'
		elif(type=='middle'):
			align = '^'
		elif(type=='middle_left'):
			align = '^'
		# 分兩邊
		# 分三份取中間

		return_body = ''
		for row in page_array:
			reviced_width = width_normalizer( self.content_width,row )
			return_body += self.row.format( content=row,align=align,wide=reviced_width )

		return return_body
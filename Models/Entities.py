from sqlalchemy import create_engine, asc, desc, Column, Integer, String, TEXT, DateTime, DECIMAL
from sqlalchemy.ext.declarative import declarative_base
from env import *
DeclarativeBase = declarative_base()

# 時間相關
import datetime
local_date = datetime.datetime.now().strftime("%Y/%m/%d")


def db_connect():

    engine = create_engine("mysql+pymysql://%s:%s@%s:%s/%s"
                        %( DB_USERNAME,DB_PASSWORD,DB_HOST,DB_PORT,DB_DATABASE ),
                        encoding=DB_ENCODING,
                        echo=False,
                        pool_recycle=1800) #定义一个指向sqlalchemy.db数据库的引擎

    return engine

#帳戶資料
class AtmAccount(DeclarativeBase):
    __tablename__ = 'atm_account'     #表的名字

    id              = Column(Integer, primary_key = True)
    account_number  = Column(String)
    password        = Column(String)
    account_type_id = Column(Integer)
    branch_id       = Column(Integer)
    customer_id     = Column(Integer)
    currency_id     = Column(Integer)
    balance         = Column(Integer)
    status          = Column(Integer)
    sys_code        = Column(String)
    updated_at      = Column(DateTime)
    created_at      = Column(DateTime)

#活動資料
class AtmAccountActivity(DeclarativeBase):
    __tablename__ = 'atm_account_activity'     #表的名字

    id          = Column(Integer, primary_key = True)
    activity_id = Column(Integer)
    account_id  = Column(Integer)
    send_mail   = Column(String)
    send_status = Column(Integer)
    count       = Column(Integer)
    lottery_num = Column(String)
    created_at  = Column(DateTime)

#分行資料
class AtmBranch(DeclarativeBase):
    __tablename__ = 'atm_branch'     #表的名字

    id          = Column(Integer, primary_key = True)
    branch_code = Column(String)
    name        = Column(String)
    add         = Column(String)
    tel         = Column(String)
    is_valid    = Column(Integer)
    created_at  = Column(DateTime)

#幣別資料
class AtmCurrency(DeclarativeBase):
    __tablename__ = 'atm_currency'     #表的名字

    id            = Column(Integer, primary_key = True)
    currency_code = Column(String)
    name_cn       = Column(String)
    name_en       = Column(String)

#顧客資料
class AtmCustomer(DeclarativeBase):
    __tablename__ = 'atm_customer'     #表的名字

    id         = Column(Integer, primary_key = True)
    name       = Column(String)
    add        = Column(String)
    tel        = Column(String)
    mail       = Column(String)
    created_at = Column(DateTime)

#交易資料
class AtmTransaction(DeclarativeBase):
    __tablename__ = 'atm_transaction'     #表的名字

    id                    = Column(Integer, primary_key = True)
    transaction_date      = Column(DateTime)
    transaction_type_id   = Column(Integer)
    account_id            = Column(Integer)
    transaction_branch_id = Column(Integer)
    transaction_account   = Column(String)
    amount                = Column(Integer)
    balance               = Column(Integer)
    sys_code              = Column(String)
    message               = Column(String)
    created_at            = Column(DateTime)


#交易類別資料
class AtmTransactionType(DeclarativeBase):
    __tablename__ = 'atm_transaction_type'     #表的名字

    id            = Column(Integer, primary_key = True)
    currency_code = Column(String)
    name_cn       = Column(String)
    name_en       = Column(String)


from sqlalchemy.orm import sessionmaker
from Entities import db_connect

class BonBonAtm:
    def __init__(self):  
        engine = db_connect()  
        # create_quotes_table(engine)  
        self.Session = sessionmaker(bind = engine)

    def get( self,TableObject ):
        session = self.Session()
        return session.query( TableObject )

    # def getValidAccount( self,AtmAccount ):
    #     session = self.Session()
    #     data = session.query( AtmAccount ).filter( AtmAccount.status==1 ).first()
    #     return data

    def updateAccountBalance( self,AtmAccount,account_id,updated_amount ):
    	session = self.Session()
    	session.query( AtmAccount ).filter( AtmAccount.id==account_id ).update( {AtmAccount.balance:updated_amount} )
    	session.commit()
import sys
from env import *
sys.path.append( PROJECT_PATH+'/Models' )

from Repositories import *
from Entities import *


class AtmAccountService():

	def __init__(self):
		self.bonBonAtm = BonBonAtm()

	def getValidAccount( self ):
		data = self.bonBonAtm.get( AtmAccount ).filter( AtmAccount.status==1 ).first()
		# data = self.bonBonAtm.getValidAccount( AtmAccount )
		return data

	#登入_驗證帳號是否有效
	def checkValidAccount ( self,account_number ) :
		return True

	#登入_驗證帳戶密碼是否正確
	def checkAccountPassword ( self,account_number, input_passwrod ) :
		return True

	#登入_新增登入log記錄
	def InsertLoginLog ( self,account_id, branch_id, status ,sys_code) :
		return True

	#帳戶相關資訊_取得field_name對應的值 (用於查詢餘額 or 其他欄位值)
	def getAccountData ( self,account_number ) : #account_id
		data = self.bonBonAtm.get( AtmAccount ).filter( AtmAccount.account_number==account_number ).first()
		return data

	# 更新帳戶餘額
	# EX.transcation_amount欄位為交易變額，存款300/提款-300
	# def updateAccountBalance ( self,account_id, transcation_amount) :
	# 	return True

	def withdrawMoney ( self,account_data,withdraw_amount ) :

		account_id = account_data.id
		updated_amount = account_data.balance-withdraw_amount

		self.bonBonAtm.updateAccountBalance( AtmAccount,account_id,updated_amount )
		return self.getValidAccount()

	def transferMoney ( self,account_data,transfer_amount ) :

		account_id = account_data.id
		updated_amount = account_data.balance-transfer_amount

		self.bonBonAtm.updateAccountBalance( AtmAccount,account_id,updated_amount )
		return self.getValidAccount()

	def depositMoney ( self,account_data,transfer_amount ) :

		account_id = account_data.id
		updated_amount = account_data.balance+transfer_amount

		self.bonBonAtm.updateAccountBalance( AtmAccount,account_id,updated_amount )
		return self.getValidAccount()


	# 新增交易資訊 (記錄成功交易的記錄)
	def insertTrasaction ( self,account_id, type_id, branch_id, amount , transcation_amount) :
		return True

	# 新增交易資訊LOG (記錄所有失敗/成功的交易)
	def insertTrasactionLog ( self,account_id, type_id, branch_id, amount , transcation_amount, status , sys_code) :
		return True

	#分行相關資訊_取得field_name對應的值 (用於分行名稱 or 其他欄位值)
	def getBranchForField ( self,branch_id, field_name) :
		return data

	#顧客相關資訊_取得field_name對應的值 (用於顧客姓名/Mail or 其他欄位值)
	def getCustomerDataForField ( self,customer_id ) :
		return data

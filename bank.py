import Controllers.MainController as MainController

# ATM Account System
class Account:


	mainController = MainController.MainController()


	def router( self,option ):

		route = {
			"login"               :self.mainController.login,
			"menu"                :self.mainController.menu,
			"show_account_balance":self.mainController.show_account_balance,
			"balance_inquiries"   :self.mainController.balance_inquiries,
			"withdrawals"         :self.mainController.withdrawals,
			"transfer"            :self.mainController.transfer,
			"deposit"             :self.mainController.deposit,
		}

		return route[option]


# Main Function
def main():

	account = Account()

	# 登入
	# next_step = account.mainController.login( account )
	action = 'login'

	while True:
		if action==False:
			break
		action = account.router( action )( account )


if __name__ == '__main__':
	main()

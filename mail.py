import sys
import smtplib
import re
import pandas as pd

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders



def mail_addr_verification(receiver_addr):
    #use regex to check valid mail format
    EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")

    if not EMAIL_REGEX.match(receiver_addr):
        return 'invalid e-mail address'

    return 'valid e-mail address'


def send_mail(receiver_addr, email_text, attached_excel):

    gmail_user = 'bonbonbank1234@gmail.com'
    # need to handle the secret password carefully, should use trendforce mail instead
    gmail_password = 'wkjmlwznyfywzkck'

    sent_from = gmail_user
    to = receiver_addr

    print(mail_addr_verification(to))

    subject = 'OMG Super Important Message'

    msg = MIMEMultipart()
    #msg = MIMEText(body, 'plain', 'utf-8')
    msg['From'] = sent_from
    msg['To'] = to
    msg['Subject'] = subject

    msg.attach(MIMEText(email_text, 'html'))


    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(attached_excel, "rb").read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename=' + attached_excel)
    msg.attach(part)

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(sent_from, to, msg.as_string())

        server.close()

        print ('Email sent!')
    except:
        print ('Something went wrong...')



def write_mail(trade_info, attached_excel_enable=False):
    # fetch email_text latest trading date information
    df_trade_info = trade_info[['姓名', '交易日期', '交易金額', '交易分行']]

    email_text = """\
    <!DOCTYPE html>
    <html>
    <head>
    <title>Page Title</title>
    </head>
    <body>

    <font color="red">
    <h1>邦邦銀行交易紀錄</h1>
    <font color="black">
    <p>親愛的""" + df_trade_info['姓名'] + """ 先生/小姐，您好！</p>
    <p>感謝您使用邦邦銀行ATM，以下是您最新的戶頭的最新帳務異動情況，請詳閱相關說明：</p>


    <table border="1">

    <tr>
    <td bgcolor="#87CEEB">交易日期</td>
    <td bgcolor="#87CEEB">交易金額</td>
    <td bgcolor="#87CEEB">交易分行</td>
    </tr>
    <tr>
    <td>""" + df_trade_info['交易日期'] + """</td>
    <td>""" + df_trade_info['交易金額'] + """</td>
    <td>""" + df_trade_info['交易分行'] + """</td>
    </tr>
    </table>

    </body>
    </html>
    """

    output_excel = None

    #df_trade_info = df_trade_info[['交易日期', '交易金額', '交易分行']]
    column_idx = ['交易日期', '交易金額', '交易型態', '交易分行', '備註']
    content = [('2017/10/15 17:31:00 AM', '3000', '存款', '建國', ''),
               ('2017/10/11 17:31:00 AM', '7000', '跨行提款', '新生', ''),
               ('2''017/10/05 13:00:00 AM', '1000', '提款', '南京', '')]

    df_email = pd.DataFrame(content, columns = column_idx)

    if attached_excel_enable == True:
        output_excel = 'attached_file.xlsx'
        writer = pd.ExcelWriter(output_excel)
        # Convert the dataframe to an XlsxWriter Excel object.
        df_email.to_excel(writer, sheet_name='Sheet1',  index = False)
        writer.save()

    return email_text, output_excel




if __name__ == '__main__':
    column_idx = ['姓名','交易日期', '交易金額', '交易型態', '交易分行', '備註']
    content = [('衛斯理', '2017/10/15 17:31:00 AM', '3000', '存款', '建國', ''),
               ('克萊兒', '2017/10/11 17:31:00 AM', '7000', '跨行提款', '新生', ''),
               ('米奇茂', '2''017/10/05 13:00:00 AM', '1000', '提款', '南京', '')]
    df_email = pd.DataFrame(content, columns = column_idx)

    index = {'衛斯理':0, '克萊兒':1, '米奇茂':2}

    trade_info = df_email.iloc[index['米奇茂'], :]

    email_text, attached_excel = write_mail(trade_info, attached_excel_enable=True)
    send_mail('richlee@trendforce.com', email_text, attached_excel)

    sys.exit()